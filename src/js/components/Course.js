import DB from "@/js/components/DB";

export default class Course {
  constructor({title, price, link, description, id, order}) {
    this.title = title;
    this.price = price;
    this.link = link;
    this.description = description;
    this.id = id;
    this.order = order;

    if (!this.constructor.removeHandler || !this.constructor.cartHandler) {
      // вызовится один раз, при создании первого экземпляра, потом вызваться не будет, потому УЖЕ в СТАТИЧЕСКУЮ переменную removeHandler добавится функция обработчик
      this.constructor.addRemoveHandler();
      this.constructor.addCartHandler();
    }
  }

  static removeHandler;
  static cartHandler;

  static addRemoveHandler() {
    this.removeHandler = function (e) {
      if (e.target.matches('[data-delete-course-id]')) {
        let data = DB.getDB();
        const id = Number(e.target.dataset.deleteCourseId);

        data.courses = data.courses.filter(el => el.id !== id)
        data.cart = data.cart.filter(el => el.idCourse !== id)
        DB.setDB(data);
      }
    }

    document.addEventListener('click', this.removeHandler)
  }

  static addCartHandler() {
    this.cartHandler = function (e) {
      if (e.target.matches('[data-cart-course-id]')) {
        let data = DB.getDB();
        const id = Number(e.target.dataset.cartCourseId);

        data.cart = [{idCourse: id, count: 1}, ...data.cart]
        DB.setDB(data);
      }
    }

    document.addEventListener('click', this.cartHandler)
  }

  render() {
    const inCart = DB.getDB().cart.find(el => el.idCourse === this.id)

    return `
      <div class="col s12 m6">
        <div class="card">
          <div class="card-image">
            <img src="${this.link}">
            <span class="card-title">${this.title}</span>
          </div>
          <div class="card-content">
            <p>${this.description}</p>
            <p class="right-align price">
              ${new Intl.NumberFormat('ru-RU', {style: 'currency', currency: 'RUB'}).format(this.price)}
            </p>
          </div>
          <div class="card-action right-align">
            <a class="green darken-1 btn-small" data-cart-course-id="${this.id}" ${!!inCart && 'disabled'}>В корзину</a>
            <a class="red darken-1 btn-small" data-delete-course-id="${this.id}">Удалить</a>
          </div>
        </div>
      </div>
    `
  }
}