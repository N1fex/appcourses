export default class DB{
  static initDB(){
    const DB = {
      courses: [],
      cart: []
    }

    this.setDB(DB)
  }

  static setDB(DB){
    localStorage.setItem('db', JSON.stringify(DB));
    // можно делать свои кастомные события и диспатчить их когда это необходимо и будет выполнятся в нужном месте кода (в этом случае в файле index.js)
    const event = new CustomEvent('DB:updated');
    document.dispatchEvent(event);
  }

  static getDB(){
    return JSON.parse(localStorage.getItem('db'));
  }
  
  static newCourse(course){
    let DB = this.getDB();

    DB.courses = [
      {id: +new Date, order: false, ...course},
      ...DB.courses
    ];
    this.setDB(DB)
  }
}