import validator from "validator/es";
import BD from "@/js/components/DB";

export default class Form {
  static errorValidate = [];

  static pushToError(target, msg) {
    this.errorValidate.push({target, msg})
  }

  static validateFunc(fields){
    this.errorValidate = [];
    if(!validator.isLength(fields.title, {min: 3, max: 20})) this.pushToError('title', 'Неверная длина названия');
    if(!validator.isNumeric(fields.price)) this.pushToError('price', 'Только цифры');
    if(!validator.isURL(fields.link)) this.pushToError('link', 'Только URL');
    if(!validator.isLength(fields.description,{min:10, max: 100})) this.pushToError('description', 'Неверная длина описания');
  }

  static clearErrors(fields) {
    Object.keys(fields).map(field => {
      let elDOM = document.getElementById(`mat-${field}`);
      elDOM.classList.remove('invalid');
      elDOM.classList.add('valid');
    })
  }

  static clearFieldsForm(fields) {
    Object.keys(fields).map(field => {
      let elDOM = document.getElementById(`mat-${field}`);
      elDOM.value = '';
      elDOM.classList.remove('valid');
    })
  }

  static addCourse(fields) {
    this.clearErrors(fields);
    this.validateFunc(fields);

    if(!this.errorValidate.length){
      BD.newCourse(fields);
      this.clearFieldsForm(fields);
      return true;
    } else {
      this.errorValidate.map(error => {
        let elDOM = document.getElementById(`mat-${error.target}`);
        elDOM.classList.add('invalid');
        elDOM.parentNode.querySelector('.helper-text').dataset.error = error.msg;
      })
      return false;
    }
  }
}