import '@/style/app.scss'
import Form from "@/js/components/Form";
import DB from "@/js/components/DB";
import Course from "@/js/components/Course";
import Cart from "@/js/components/Cart";

// MATERIALIZE
// для работы мобильного навбара
document.addEventListener('DOMContentLoaded', function() {
  let elems = document.querySelectorAll('.sidenav');
  let instances = M.Sidenav.init(elems);
});

// для работы модального окна
document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.modal');
  var instances = M.Modal.init(elems);
});

// ---------MATERIALIZE-------------



// если база пустая то инициализируем ее
if(!DB.getDB()) DB.initDB();


// !переключение между страницами!
function routePage(route) {
  document.getElementById('progress').style.display = 'block';

  setTimeout(() => {
    // toggle pages
    [...document.querySelectorAll('.main-block')].map(el => el.style.display = 'none');
    document.querySelector(`[data-block="main-block--${route}"]`).style.display = 'block';

    // class for links
    [...linkRoutes].map(link => link.parentElement.classList.remove('active'));
    document.querySelector(`[data-route=${route}]`).parentElement.classList.add('active');

    document.getElementById('progress').style.display = 'none';
  }, getRandomInt(2000))
}

let linkRoutes = document.querySelectorAll('[data-route]');
[...linkRoutes].map(link => {
  link.addEventListener('click', e => {
    routePage(link.getAttribute('data-route'));
  })
});
// ---------- !переключение между страницами! ---------

// Добавление нового курса
let addCourseButton = document.getElementById('add-course');
addCourseButton.addEventListener('click', () => {
  let filedsForm = {
    title: document.getElementById('mat-title').value,
    price: document.getElementById('mat-price').value + '', // так нужно, потому что validator принимает только строку!
    link: document.getElementById('mat-link').value,
    description: document.getElementById('mat-description').value,
  }
  
  let successAddCourse = Form.addCourse(filedsForm);

  if(successAddCourse){
    routePage('all');
    updateApp();
  }
})
// ------------- Добавление нового курса --------------


// Всякие обновления
updateApp();
document.addEventListener('DB:updated', updateApp);
function updateApp(){
  let courses = DB.getDB().courses;
  let cart = DB.getDB().cart;

  // обновляем список курсов
  let listCoursesBlock = document.getElementById('list-courses');
  listCoursesBlock.innerHTML = '';

  if(!courses.length){
    listCoursesBlock.innerHTML = '<p class="center-align">Курсов пока нету</p>'
  } else {
    courses.map(el => {
      let course = new Course(el);
      listCoursesBlock.insertAdjacentHTML('beforeend', course.render());
    })
  }

  //обновляем корзину
  let listCartBlock = document.getElementById('list-cart');
  let optionsCartBlock = document.getElementById('cart-options');
  let totalPriceCartBlock = document.getElementById('total-price-cart');
  let totalPriceModalBlock = document.getElementById('total-price-modal');

  listCartBlock.innerHTML = '';

  if(!cart.length){
    listCartBlock.innerHTML = '<p class="center-align">Корзина пуста</p>';
    optionsCartBlock.style.display = 'none';
  } else {
    cart.map(el => {
      let courseCart = new Cart(el);
      listCartBlock.insertAdjacentHTML('beforeend', courseCart.render());

      totalPriceCartBlock.innerHTML = Cart.totalPrice();
      totalPriceModalBlock.innerHTML = Cart.totalPrice();
      optionsCartBlock.style.display = 'block';
    })
  }
}

// helper
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}



